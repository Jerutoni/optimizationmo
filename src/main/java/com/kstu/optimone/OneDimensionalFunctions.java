package com.kstu.optimone;

import static java.lang.Math.abs;
import static java.lang.Math.exp;

public class OneDimensionalFunctions {

    public static void main(String[] args) {
        double epsilon = 1e-2;
        double a = -7, b = 11;
        System.out.println(findMax(a, b, epsilon));
        System.out.println(distributionOfPoints(a, b, 0.01));
        System.out.println(dichotomy(a, b, epsilon));
    }

    //корень дробления
    static final double PHI = (1 + Math.sqrt(5)) / 2;

    static double f(double x) {
        return exp(-1.5 * x) + x - 2;
    }

    //Золотое сечение
    static double findMax(double a, double b, double eps) {
        double x1, x2;
        while (true) {
            x1 = b - (b - a) / PHI;
            x2 = a + (b - a) / PHI;
            if (f(x1) <= f(x2))
                a = x1;
            else
                b = x2;
            if (Math.abs(b - a) < eps)
                break;
        }

        return (a + b) / 2;
    }

    // Дихотомия
    static double dichotomy(double a, double b, double eps) {
        double c, x = 0;

        while (abs(b - a) > eps) {
            c = (a + b) / 2;
            if (f(a) * f(c) <= 0) b = c;
            else {
                a = c;
                x = (a + b) / 2;
            }
        }
        return x;
    }

    // Распределение точек
    static double distributionOfPoints(double a, double b, double step) {
        double yMax = 0;

        while (a < b) {

            if (yMax < f(a)) {

                yMax = f(a);

            }
            a += step;
        }
        return yMax;
    }
}