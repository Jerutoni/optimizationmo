package com.kstu.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Утилитарный класс для работы с ячейками массива
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Cell {
    private Integer i;

    private Integer j;

    private String sign;

    public Cell(Integer i, Integer j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "i=" + i +
                ", j=" + j +
                ", sign='" + sign + '\'' +
                '}';
    }
}
