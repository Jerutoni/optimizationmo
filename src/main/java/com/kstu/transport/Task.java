package com.kstu.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Task {
    // Потребности
    private Integer[] horizon;
    // Запасы
    private Integer[] vertical;
    // Пункты отправления -> Пункты назначения
    private Integer[][] table;
    private Integer[][] plan;

    public Task(Integer[] horizon, Integer[] vertical, Integer[][] table) {
        this.horizon = horizon;
        this.vertical = vertical;
        this.table = table;
        if (!isClosedTask()) {
            throw new IllegalArgumentException("Задача открытого типа");
        }
    }

    public Integer runTask() {
        makeFirsPlan();
        potentialMethod();
        int sum = 0;
        for (int i = 0; i < plan.length; i++) {
            for (int j = 0; j < plan[i].length; j++) {
                sum = sum + (table[i][j] * plan[i][j]);
            }
        }
        return sum;
    }

    // Составляем опорный план методом северно-западного угла
    private void makeFirsPlan() {

        plan = new Integer[table.length][table[0].length];

        for (Integer[] integers : plan) Arrays.fill(integers, 0);

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (vertical[i] != 0) {
                    if (vertical[i] > horizon[j]) {
                        int b = horizon[j];
                        vertical[i] = vertical[i] - horizon[j];
                        horizon[j] = 0;
                        plan[i][j] = b;
                    }
                    if (vertical[i] <= horizon[j]) {
                        plan[i][j] = vertical[i];
                        horizon[j] = horizon[j] - vertical[i];
                        vertical[i] = 0;
                    }
                }
            }
        }
    }

    private void potentialMethod() {
        Integer[][] potentialMatrix = new Integer[plan.length][plan[0].length];

        for (Integer[] integers : potentialMatrix) Arrays.fill(integers, 0);

        String horizonChar = "B";
        String verticalChar = "A";

        List<Equation> equations = new ArrayList<>();

        // Составляем систему линейных уравнений
        for (int i = 0; i < plan.length; i++) {
            for (int j = 0; j < plan[i].length; j++) {
                if (plan[i][j] != 0) {
                    equations.add(new Equation(horizonChar + j, verticalChar + i, table[i][j]));
                }
            }
        }

        checkPlan(equations);

        //Высчитываем потенциалы пунктов отправления и назначения
        Map<String, Integer> potentialMap = solveSystemOfEquations(equations);

        Map<String, Integer> map = new LinkedHashMap<>();

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < plan.length; i++) {
            for (int j = 0; j < plan[i].length; j++) {
                if (plan[i][j] == 0) {
                    int value = potentialMap.get("B" + j) - potentialMap.get("A" + i) - table[i][j];
                    potentialMatrix[i][j] = value;
                    if (value > max) max = value;
                    map.put(i + " " + j, value);
                }
            }
        }

        long count = map.values().stream().filter(i -> i > 0).count();
        boolean isOptimal = false;

        if (count == 0) {
            isOptimal = true;
        }

        if (!isOptimal) {
            List<Cell> cells = new ArrayList<>();
            for (int i = 0; i < potentialMatrix.length; i++) {
                for (int j = 0; j < potentialMatrix[0].length; j++) {
                    if (potentialMatrix[i][j].equals(max)) {
                        cells = recalculation(i, j);
                    }
                }
            }
            upgradeFirstPlan(cells);
            potentialMethod();
        }
    }

    //Проверяем на вырожденность
    private void checkPlan(List<Equation> equations) {
        int rightCount = vertical.length + horizon.length - 1;
        int ourCount = 0;
        for (Integer[] i : plan) {
            for (Integer j : i) {
                if (j != 0) ourCount++;
            }
        }

        //А если вырождение не в одном месте?То тут List должен быть?
        int verticalIndex = -1;

        if (rightCount != ourCount) {
            int val = rightCount / 2;
            for (int i = 0; i < plan.length; i++) {
                int countNonNull = 0;
                for (int j = 0; j < plan[0].length; j++) {
                    if (plan[i][j] != 0) countNonNull++;
                }
                if (countNonNull != val) verticalIndex = i;
            }
        }

        if (verticalIndex != -1) {
            for (int j = 0; j < plan[0].length; j++) {
                if (plan[verticalIndex][j] > 0) {
                    if (j + 1 < plan[0].length - 1) {
                        int index = j+1;
                        equations.add(new Equation("B" + index, "A" + verticalIndex, table[verticalIndex][j+1]));
                        break;
                    } else {
                        int index = j - 1;
                        equations.add(new Equation("B" + index, "A" + verticalIndex, table[verticalIndex][j-1]));
                        break;
                    }
                }
            }
        }
    }


    // Цикл перерасчета
    private List<Cell> recalculation(Integer vertical, Integer horizontal) {
        List<Cell> horizonCells = new ArrayList<>();
        List<Cell> verticalCells = new ArrayList<>();

        // По вертикали
        for (int i = 0; i < plan.length; i++) {
            if (plan[i][horizontal] != 0) {
                verticalCells.add(new Cell(i, horizontal));
            }
        }

        // По горизонтали
        for (int i = 0; i < plan[0].length; i++) {
            if (plan[vertical][i] != 0) {
                horizonCells.add(new Cell(vertical, i));
            }
        }


        List<Cell> cells = new ArrayList<>();


        for (Cell h : horizonCells) {
            for (Cell v : verticalCells) {
                if (plan[v.getI()][h.getJ()] != 0) {
                    Sign sign = new Sign();
                    Cell c1 = new Cell(vertical, horizontal, sign.getSign());
                    h.setSign(sign.getSign());
                    Cell c2 = new Cell(v.getI(), h.getJ(), sign.getSign());
                    v.setSign(sign.getSign());
                    cells.addAll(Arrays.asList(c1, v, h, c2));
                }
            }
        }

        if (cells.size() % 2 != 0) {
            throw new RuntimeException("Ошибка в перерасчете");
        }

        //Если не получилось составить цикл со всеми заполненными вершинами
        if (cells.isEmpty()) {
            Sign sign = new Sign();
            Cell c1 = new Cell(vertical, horizontal, sign.getSign());
            Cell c2 = horizonCells.get(0);
            Cell c4 = verticalCells.get(0);
            c2.setSign(sign.getSign());
            Cell c3 = new Cell(c4.getI(), c2.getJ(), sign.getSign());
            c4.setSign(sign.getSign());
            cells.addAll(Arrays.asList(c1, c2, c3, c4));
        }
        return cells;
    }


    private void upgradeFirstPlan(List<Cell> cells) {
        Integer min = Integer.MAX_VALUE;
        for (Cell cell : cells) {
            if (cell.getSign().equalsIgnoreCase("-") && plan[cell.getI()][cell.getJ()] < min) {
                min = plan[cell.getI()][cell.getJ()];
            }
        }


        for (Cell cell : cells) {
            Integer val = plan[cell.getI()][cell.getJ()];
            if (cell.getSign().equalsIgnoreCase("+")) {
                plan[cell.getI()][cell.getJ()] = val + min;
            } else {
                plan[cell.getI()][cell.getJ()] = val - min;
            }
        }
    }

    // Решаем систему уравнений
    private Map<String, Integer> solveSystemOfEquations(List<Equation> equations) {
        Map<String, Integer> potentialMap = new LinkedHashMap<>();
        potentialMap.put("A0", 0); // Допустим,что A0 = 0
        //check все значения в мапе,пока они не в мапе
        while (true) {
            for (Equation equation : equations) {
                Integer x1 = potentialMap.get(equation.getX1());
                Integer x2 = potentialMap.get(equation.getX2());

                if ((x1 != null && x2 != null)) continue;

                if (x1 == null) {
                    if (x2 == null) {
                        continue;
                    }
                    potentialMap.put(equation.getX1(), equation.getValue() - (-x2));
                } else {
                    potentialMap.put(equation.getX2(), Objects.requireNonNullElse(x2, x1) - equation.getValue());
                }
            }
            boolean isSolved = isSolved(equations, potentialMap.keySet());
            if (isSolved) break;
        }
        return potentialMap;
    }

    private boolean isSolved(List<Equation> equations, Set<String> roots) {
        Set<String> setToCheck = new HashSet<>();
        for (Equation eq : equations) {
            setToCheck.add(eq.getX1());
            setToCheck.add(eq.getX2());
        }
        boolean isSolved = true;
        //Если хоть одно не нашлось - false
        for (String s : setToCheck) {
            if (!roots.contains(s)) {
                isSolved = false;
                break;
            }
        }
        return isSolved;
    }

    // Проверяем на открытось / закрытость
    private boolean isClosedTask() {
        int sumVertical = Arrays.stream(vertical)
                .mapToInt(Integer::intValue)
                .sum();
        int sumHorizon =Arrays.stream(horizon)
                .mapToInt(Integer::intValue)
                .sum();
        return sumHorizon - sumVertical == 0;

    }

    @Override
    public String toString() {
        return Arrays.deepToString(plan);
    }
}
