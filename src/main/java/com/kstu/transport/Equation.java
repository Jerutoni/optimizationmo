package com.kstu.transport;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Утилитарный класс для уравнений
*/
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Equation {
    private String x1;
    private String x2;
    private Integer value;

    @Override
    public String toString() {
        return x1 + "-" + x2 + "=" + value;
    }
}
